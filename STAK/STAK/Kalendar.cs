﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using Hangfire;
using Hangfire.SqlServer;

namespace STAK
{
    public partial class Kalendar : Form
    {
        DateTime odabraniDatum = DateTime.Now;
        string poveznica = @"Data Source=31.147.204.119\PISERVER,1433;Initial Catalog=17033_DB;Persist Security Info=True;User ID=17033_User;Password=EJsRauDq";
        public Kalendar()
        {
            InitializeComponent();
            //konfiguracija hangfire servera

            JobStorage.Current = new SqlServerStorage(poveznica);
            GlobalConfiguration.Configuration.UseSqlServerStorage(poveznica);

            //prikazuje se samo jedan kalendar
            prikazKalendara.MaxSelectionCount = 1;
            

            //prikaz trenutno odabranog datuma

            prikazOdabranogDatuma.Text = prikazKalendara.SelectionRange.Start.ToShortDateString();
            this.eventTableAdapter.FillByDate(this._17033_DBDataSet1.Event, prikazOdabranogDatuma.Text);


        }

        private void prikazKalendara_DateChanged(object sender, DateRangeEventArgs e)
        {
            
            odabraniDatum = prikazKalendara.SelectionRange.Start.Date;
            prikazOdabranogDatuma.Text = odabraniDatum.ToShortDateString();

            try
            {
                //(prikazEventova.DataSource as DataTable).DefaultView.RowFilter = upitNaBazu;
                this.eventTableAdapter.FillByDate(this._17033_DBDataSet1.Event, prikazOdabranogDatuma.Text );
            }
            catch
            {
                // prikazEventova.DataSource = null;
                //prikazEventova.Refresh();
                this.eventTableAdapter.Fill(this._17033_DBDataSet1.Event );
            }
            

            //this.eventTableAdapter.Fill(this._17033_DBDataSet1.Event);
        }

        private void dodajEvent_Click(object sender, EventArgs e)
        {
            //Dohvati sve podatke s forme
            if (imeEventa.Text == "" || vrijemePocetkaEventa.Text == "" || vrijemeZavrsetkaEventa.Text == "" || opisEventa.Text == "" ||  lokacijaEventa.Text == "")
            {
                MessageBox.Show("Morate popuniti sva polja!");
                return;
            }

            DateTime datum = DateTime.Parse(prikazOdabranogDatuma.Text);
            
            string naziv = imeEventa.Text;
            if (imeEventa.Text[0] != (char)imeEventa.Text.ToString().ToUpper()[0])
            {
                MessageBox.Show("Ime mora započinjati velikim slovom!");
                return;
            }
            DateTime upisaniPocetak = DateTime.Parse(vrijemePocetkaEventa.Text);
            DateTime pocetak = datum.Date + upisaniPocetak.TimeOfDay;
            if (pocetak < DateTime.Now)
            {
                MessageBox.Show("Ne možete dodati događaj u prošlost!");
                return;
            }
            DateTime upisaniZavrsetak = DateTime.Parse(vrijemeZavrsetkaEventa.Text);
            DateTime zavrsetak = datum.Date + upisaniZavrsetak.TimeOfDay;
            if (zavrsetak < pocetak)
            {
                MessageBox.Show("Događaj ne može završiti prije početka!");
                return;
            }
            string opis = opisEventa.Text;
            string lokacija = lokacijaEventa.Text;
            Podsjetnici odabraniPodsjetnik = (Podsjetnici) podsjetnikEventa.SelectedValue;


            //ovisno o odabranom podsjetniku, izračunaj vrijeme za hangfire

            DateTime vrijemePodsjetnika = pocetak.AddMinutes(odabraniPodsjetnik.vrijednostPodsjetnika);
            DateTime vrijemeSada = DateTime.Now;
            
            int minuteDoPodsjetnika = (int) (vrijemePodsjetnika.Subtract(vrijemeSada).TotalMinutes);
            
            
            //triggeraj ifttt sa hangfire

            WebClient webKlijent = new WebClient();
            JobStorage.Current = new SqlServerStorage(poveznica);

            var options = new BackgroundJobServerOptions
            {
                SchedulePollingInterval = TimeSpan.FromSeconds(1)
            };

            var server = new BackgroundJobServer(options);
            
           

            switch (odabraniPodsjetnik.imePodsjetnika)
            {
                case "Minutu prije":
                    
                        try
                        {
                            BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/minutu_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                        }
                        catch
                        {
                            minuteDoPodsjetnika = 60;
                            BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/minutu_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                        }
                    break;
                case "Dva sata prije":
                    try
                    {
                        BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/dva_sata_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                    }
                    catch
                    {
                        minuteDoPodsjetnika = 60;
                        BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/dva_sata_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                    }
                    break;
                case "Sat prije":
                    try
                    {
                        BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/sat_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                    }
                    catch
                    {
                        minuteDoPodsjetnika = 60;
                        BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/sat_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                    }
                    break;
                case "Dan prije":
                    try
                    {
                        BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/dan_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                    }
                    catch
                    {
                        minuteDoPodsjetnika = 60;
                        BackgroundJob.Schedule(() => webKlijent.OpenReadAsync(new Uri("https://maker.ifttt.com/trigger/dan_prije/with/key/fA0lwCa6wuq8TIAy85yaavEFRDp1rAtlH_KShJ96HIo")), TimeSpan.FromMinutes(minuteDoPodsjetnika));

                    }
                    break;
                default:
                    break;
            }   

            //dodaj event u bazu

            Dogadaj dogadaj = new Dogadaj(datum, naziv, pocetak, zavrsetak, opis, lokacija, vrijemePodsjetnika);
            if (dogadaj.Spremi())
            {
                MessageBox.Show("Uspješno dodan event u bazu!");
                this.eventTableAdapter.FillByDate(this._17033_DBDataSet1.Event, prikazOdabranogDatuma.Text);
                eventDataGridView.Update();
                eventDataGridView.Refresh();
            }


        }

        private void Kalendar_Load(object sender, EventArgs e)
        {
            List<Dogadaj> datumiEventa = Dogadaj.DohvatiSveDogadaje();
            
            List<Podsjetnici> listica = Podsjetnici.listaPodsjetnika;
            

                foreach (var item in datumiEventa)
                {
                    prikazKalendara.AddBoldedDate(item.datum);
                }
            prikazKalendara.UpdateBoldedDates();

            podsjetnikEventa.DisplayMember = "ime";
            podsjetnikEventa.DataSource = listica;

        }
        

        private void povratakNaGlavnuFormu_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlavnaForma forma = new GlavnaForma();
            forma.Show();
        }

        

        private void Kalendar_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"https://github.com/foivz/r17033/wiki/Korisni%C4%8Dka-dokumentacija", "kalendar");

        }
    }
}
