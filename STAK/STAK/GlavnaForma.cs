﻿using System;
using System.Windows.Forms;

namespace STAK
{
    public partial class GlavnaForma : Form
    {
        public GlavnaForma()
        {
            InitializeComponent();
        }

        private void GlavnaForma_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"https://github.com/foivz/r17033/wiki/Korisni%C4%8Dka-dokumentacija", "glavna-forma");
        }

        private void idiNaKalendar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Kalendar forma = new Kalendar();
            forma.Show();
        }

        private void idiNaSlanjeObavijesti_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormaSlanjeObavijesti forma = new FormaSlanjeObavijesti();
            forma.Show();
        }



        private void idiNaForum_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormaForumPocetna forma = new FormaForumPocetna();
            forma.Show();
        }

        private void idiNaUrednickuAdministraciju_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormaUrednickaAdministracija forma = new FormaUrednickaAdministracija();
            forma.Show();
        }

        private void idiNaDodijeljeneTeme_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormaDodijeljeneTeme forma = new FormaDodijeljeneTeme();
            forma.Show();
        }

        private void GlavnaForma_Load(object sender, EventArgs e)
        {
            if(PrijavljeniKorisnik.idTipaKorisnika != 1)
            {
                idiNaUrednickuAdministraciju.Hide();
            }
            
        }

        private void odjaviSe_Click(object sender, EventArgs e)
        {
            PrijavljeniKorisnik.idKorisnika = 0;
            PrijavljeniKorisnik.email = "";
            PrijavljeniKorisnik.ime = "";
            PrijavljeniKorisnik.prezime = "";
            PrijavljeniKorisnik.idTipaKorisnika = 0;

            this.Hide();
            FormaPrijave forma = new FormaPrijave();
            forma.Show();
        }
    }
}
