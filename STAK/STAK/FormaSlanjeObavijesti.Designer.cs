﻿namespace STAK
{
    partial class FormaSlanjeObavijesti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaSlanjeObavijesti));
            this.PovratakNaGlavniIzbornik = new System.Windows.Forms.Button();
            this.SadrzajObavijesti = new System.Windows.Forms.TextBox();
            this.PrimateljiObavijesti = new System.Windows.Forms.ComboBox();
            this.Obavijest = new System.Windows.Forms.Label();
            this.PrimateljObavijesti = new System.Windows.Forms.Label();
            this.PosaljiObavijest = new System.Windows.Forms.Button();
            this.NaslovObavijesti = new System.Windows.Forms.Label();
            this.NaslovObavijestiUnos = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // PovratakNaGlavniIzbornik
            // 
            this.PovratakNaGlavniIzbornik.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PovratakNaGlavniIzbornik.Location = new System.Drawing.Point(197, 29);
            this.PovratakNaGlavniIzbornik.Margin = new System.Windows.Forms.Padding(2);
            this.PovratakNaGlavniIzbornik.Name = "PovratakNaGlavniIzbornik";
            this.PovratakNaGlavniIzbornik.Size = new System.Drawing.Size(99, 66);
            this.PovratakNaGlavniIzbornik.TabIndex = 9;
            this.PovratakNaGlavniIzbornik.Text = "POVRATAK NA GLAVNI IZBORNIK";
            this.PovratakNaGlavniIzbornik.UseVisualStyleBackColor = true;
            this.PovratakNaGlavniIzbornik.Click += new System.EventHandler(this.PovratakNaGlavniIzbornik_Click);
            // 
            // SadrzajObavijesti
            // 
            this.SadrzajObavijesti.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SadrzajObavijesti.Location = new System.Drawing.Point(197, 221);
            this.SadrzajObavijesti.Margin = new System.Windows.Forms.Padding(2);
            this.SadrzajObavijesti.Multiline = true;
            this.SadrzajObavijesti.Name = "SadrzajObavijesti";
            this.SadrzajObavijesti.Size = new System.Drawing.Size(486, 310);
            this.SadrzajObavijesti.TabIndex = 10;
            // 
            // PrimateljiObavijesti
            // 
            this.PrimateljiObavijesti.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PrimateljiObavijesti.FormattingEnabled = true;
            this.PrimateljiObavijesti.Location = new System.Drawing.Point(429, 60);
            this.PrimateljiObavijesti.Margin = new System.Windows.Forms.Padding(2);
            this.PrimateljiObavijesti.Name = "PrimateljiObavijesti";
            this.PrimateljiObavijesti.Size = new System.Drawing.Size(254, 21);
            this.PrimateljiObavijesti.TabIndex = 11;
            // 
            // Obavijest
            // 
            this.Obavijest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Obavijest.AutoSize = true;
            this.Obavijest.BackColor = System.Drawing.Color.Transparent;
            this.Obavijest.ForeColor = System.Drawing.Color.White;
            this.Obavijest.Location = new System.Drawing.Point(203, 176);
            this.Obavijest.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Obavijest.Name = "Obavijest";
            this.Obavijest.Size = new System.Drawing.Size(130, 13);
            this.Obavijest.TabIndex = 12;
            this.Obavijest.Text = "Unesite zeljenu obavijest: ";
            // 
            // PrimateljObavijesti
            // 
            this.PrimateljObavijesti.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PrimateljObavijesti.AutoSize = true;
            this.PrimateljObavijesti.BackColor = System.Drawing.Color.Transparent;
            this.PrimateljObavijesti.ForeColor = System.Drawing.Color.White;
            this.PrimateljObavijesti.Location = new System.Drawing.Point(426, 29);
            this.PrimateljObavijesti.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PrimateljObavijesti.Name = "PrimateljObavijesti";
            this.PrimateljObavijesti.Size = new System.Drawing.Size(39, 13);
            this.PrimateljObavijesti.TabIndex = 13;
            this.PrimateljObavijesti.Text = "Prima: ";
            // 
            // PosaljiObavijest
            // 
            this.PosaljiObavijest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PosaljiObavijest.Location = new System.Drawing.Point(718, 499);
            this.PosaljiObavijest.Margin = new System.Windows.Forms.Padding(2);
            this.PosaljiObavijest.Name = "PosaljiObavijest";
            this.PosaljiObavijest.Size = new System.Drawing.Size(70, 32);
            this.PosaljiObavijest.TabIndex = 14;
            this.PosaljiObavijest.Text = "Pošalji";
            this.PosaljiObavijest.UseVisualStyleBackColor = true;
            this.PosaljiObavijest.Click += new System.EventHandler(this.PosaljiObavijest_Click);
            // 
            // NaslovObavijesti
            // 
            this.NaslovObavijesti.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NaslovObavijesti.AutoSize = true;
            this.NaslovObavijesti.BackColor = System.Drawing.Color.Transparent;
            this.NaslovObavijesti.ForeColor = System.Drawing.Color.White;
            this.NaslovObavijesti.Location = new System.Drawing.Point(203, 132);
            this.NaslovObavijesti.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.NaslovObavijesti.Name = "NaslovObavijesti";
            this.NaslovObavijesti.Size = new System.Drawing.Size(127, 13);
            this.NaslovObavijesti.TabIndex = 18;
            this.NaslovObavijesti.Text = "Unesite naslov obavijesti:";
            // 
            // NaslovObavijestiUnos
            // 
            this.NaslovObavijestiUnos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NaslovObavijestiUnos.Location = new System.Drawing.Point(429, 132);
            this.NaslovObavijestiUnos.Margin = new System.Windows.Forms.Padding(2);
            this.NaslovObavijestiUnos.Name = "NaslovObavijestiUnos";
            this.NaslovObavijestiUnos.Size = new System.Drawing.Size(254, 20);
            this.NaslovObavijestiUnos.TabIndex = 19;
            // 
            // FormaSlanjeObavijesti
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.NaslovObavijestiUnos);
            this.Controls.Add(this.NaslovObavijesti);
            this.Controls.Add(this.PosaljiObavijest);
            this.Controls.Add(this.PrimateljObavijesti);
            this.Controls.Add(this.Obavijest);
            this.Controls.Add(this.PrimateljiObavijesti);
            this.Controls.Add(this.SadrzajObavijesti);
            this.Controls.Add(this.PovratakNaGlavniIzbornik);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormaSlanjeObavijesti";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SlanjeObavijesti";
            this.Load += new System.EventHandler(this.FormaSlanjeObavijesti_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FormaSlanjeObavijesti_HelpRequested);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PovratakNaGlavniIzbornik;
        private System.Windows.Forms.TextBox SadrzajObavijesti;
        private System.Windows.Forms.ComboBox PrimateljiObavijesti;
        private System.Windows.Forms.Label Obavijest;
        private System.Windows.Forms.Label PrimateljObavijesti;
        private System.Windows.Forms.Button PosaljiObavijest;
        private System.Windows.Forms.Label NaslovObavijesti;
        private System.Windows.Forms.TextBox NaslovObavijestiUnos;
    }
}