﻿namespace STAK
{
    partial class FormaNoviPost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaNoviPost));
            this.naslovNoviPost = new System.Windows.Forms.Label();
            this.tekstPosta = new System.Windows.Forms.RichTextBox();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.sadrzajNoviPost = new System.Windows.Forms.Label();
            this.naslovPosta = new System.Windows.Forms.TextBox();
            this.objaviPost = new System.Windows.Forms.Button();
            this.naslovNovogPosta = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // naslovNoviPost
            // 
            this.naslovNoviPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.naslovNoviPost.AutoSize = true;
            this.naslovNoviPost.BackColor = System.Drawing.Color.Transparent;
            this.naslovNoviPost.Font = new System.Drawing.Font("Roboto Condensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.naslovNoviPost.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.naslovNoviPost.Location = new System.Drawing.Point(220, 40);
            this.naslovNoviPost.Name = "naslovNoviPost";
            this.naslovNoviPost.Size = new System.Drawing.Size(137, 38);
            this.naslovNoviPost.TabIndex = 1;
            this.naslovNoviPost.Text = "Novi post";
            // 
            // tekstPosta
            // 
            this.tekstPosta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tekstPosta.Location = new System.Drawing.Point(227, 244);
            this.tekstPosta.Name = "tekstPosta";
            this.tekstPosta.Size = new System.Drawing.Size(475, 232);
            this.tekstPosta.TabIndex = 2;
            this.tekstPosta.Text = "";
            // 
            // sadrzajNoviPost
            // 
            this.sadrzajNoviPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sadrzajNoviPost.AutoSize = true;
            this.sadrzajNoviPost.BackColor = System.Drawing.Color.Transparent;
            this.sadrzajNoviPost.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sadrzajNoviPost.Location = new System.Drawing.Point(224, 228);
            this.sadrzajNoviPost.Name = "sadrzajNoviPost";
            this.sadrzajNoviPost.Size = new System.Drawing.Size(71, 13);
            this.sadrzajNoviPost.TabIndex = 3;
            this.sadrzajNoviPost.Text = "Sadržaj posta";
            // 
            // naslovPosta
            // 
            this.naslovPosta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.naslovPosta.Location = new System.Drawing.Point(227, 179);
            this.naslovPosta.Name = "naslovPosta";
            this.naslovPosta.Size = new System.Drawing.Size(475, 20);
            this.naslovPosta.TabIndex = 4;
            // 
            // objaviPost
            // 
            this.objaviPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.objaviPost.BackColor = System.Drawing.SystemColors.Control;
            this.objaviPost.Location = new System.Drawing.Point(660, 498);
            this.objaviPost.Name = "objaviPost";
            this.objaviPost.Size = new System.Drawing.Size(105, 23);
            this.objaviPost.TabIndex = 5;
            this.objaviPost.Text = "Objavi";
            this.objaviPost.UseVisualStyleBackColor = false;
            // 
            // naslovNovogPosta
            // 
            this.naslovNovogPosta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.naslovNovogPosta.AutoSize = true;
            this.naslovNovogPosta.BackColor = System.Drawing.Color.Transparent;
            this.naslovNovogPosta.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.naslovNovogPosta.Location = new System.Drawing.Point(224, 152);
            this.naslovNovogPosta.Name = "naslovNovogPosta";
            this.naslovNovogPosta.Size = new System.Drawing.Size(69, 13);
            this.naslovNovogPosta.TabIndex = 6;
            this.naslovNovogPosta.Text = "Naslov posta";
            // 
            // FormaNoviPost
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.naslovNovogPosta);
            this.Controls.Add(this.objaviPost);
            this.Controls.Add(this.naslovPosta);
            this.Controls.Add(this.sadrzajNoviPost);
            this.Controls.Add(this.tekstPosta);
            this.Controls.Add(this.naslovNoviPost);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormaNoviPost";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormaNoviPost";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label naslovNoviPost;
        private System.Windows.Forms.RichTextBox tekstPosta;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.Label sadrzajNoviPost;
        private System.Windows.Forms.TextBox naslovPosta;
        private System.Windows.Forms.Button objaviPost;
        private System.Windows.Forms.Label naslovNovogPosta;
    }
}