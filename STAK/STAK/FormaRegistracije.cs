﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace STAK
{
    public partial class FormaRegistracije : Form
    {
        
        public FormaRegistracije()
        {
            InitializeComponent();
        }

        private void gumbRegistracije_Click(object sender, EventArgs e)
        {
            //validacija unosa

            if (inputEmail.Text == "" || inputLozinka.Text == "" || inputIme.Text == "" || inputPrezime.Text == "")
            {
                MessageBox.Show("Morate upisati ime, prezime, email i lozinku!");
                return;
            }
            if(inputIme.Text[0] != (char)inputIme.Text.ToString().ToUpper()[0])
            {
                MessageBox.Show("Ime mora započinjati velikim slovom!");
                return;
            }
            if (inputPrezime.Text[0] != (char)inputPrezime.Text.ToString().ToUpper()[0])
            {
                MessageBox.Show("Prezime mora započinjati velikim slovom!");
                return;
            }
            try
            {
                var provjeraValidnostiMaila = new System.Net.Mail.MailAddress(inputEmail.Text);
                
            }
            catch
            {
                MessageBox.Show("Email nije u pravilnom formatu!");
                return;
            }
            
            var sadrziZnamenku = new Regex(@"[0-9]+");
            var sadrziVelikoSlovo = new Regex(@"[A-Z]+");
            var sadrziMin5znakova = new Regex(@".{5,}");

            if(!(sadrziZnamenku.IsMatch(inputLozinka.Text) && sadrziVelikoSlovo.IsMatch(inputLozinka.Text) && sadrziMin5znakova.IsMatch(inputLozinka.Text)))
            {
                MessageBox.Show("Lozinka mora sadrzavati minimalno 5 znakova, veliko slovo i znamenku!");
                return;
            }
            


            if (inputPonovljeneLozinke.Text !=  inputLozinka.Text)
            {
                oznakaGreske.Show();
                oznakaGreske.Text = "Ne poklapa se!";
                return;
            }

            //registracija korisnika i povratak na prijavu

            try
            {

                Korisnik noviKorisnik = new Korisnik(inputEmail.Text, inputPonovljeneLozinke.Text, inputIme.Text, inputPrezime.Text);
                if (noviKorisnik.Spremi())
                {
                    MessageBox.Show("Uspješna registracija!");
                    this.Hide();
                    FormaPrijave forma = new FormaPrijave();
                    forma.Show();
                }
                else
                {
                    MessageBox.Show("Već postoji korisnik sa unesenom email adresom!");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void RegistrationForm_Load(object sender, EventArgs e)
        {
            oznakaGreske.Hide();
            inputLozinka.PasswordChar = '*';
            inputPonovljeneLozinke.PasswordChar = '*';
        }

        private void odustani_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormaPrijave forma = new FormaPrijave();
            forma.Show();
        }

        private void FormaRegistracije_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"https://github.com/foivz/r17033/wiki/Korisni%C4%8Dka-dokumentacija", "registracija");

        }
    }
}
