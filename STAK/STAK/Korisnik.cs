﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;

namespace STAK
{
    public class Korisnik
    {
        
        #region Constructors
       
        public Korisnik()
        {

        }
        
        public Korisnik(DbDataReader dr)
        {
            if (dr != null)
            {
                Id = int.Parse(dr["idUser"].ToString());
                Ime = dr["ime"].ToString();
                Prezime = dr["prezime"].ToString();
                Email = dr["mail"].ToString();
                Lozinka = dr["lozinka"].ToString();
                PotvrdenKorisnik = int.Parse(dr["isVerified"].ToString());
                IdTipaKorisnika = int.Parse(dr["idUserType"].ToString());

            }
        }
        //konstruktor za registraciju
        public Korisnik(string mail, string lozinka, string ime, string prezime)
        {
            Ime = ime;
            Prezime = prezime;
            Email = mail;
            Lozinka = lozinka;
        }
        public Korisnik(string mail, string lozinka)
        {
            Email = mail;
            Lozinka = lozinka;
        }
        #endregion
        #region Private Fields
        private int idKorisnika;
        private string ime;
        private string prezime;
        private string email;
        private string lozinka;
        private int potvrdenKorisnik;
        private int idTipaKorisnika;
        #endregion
        #region Properties
    
        public int Id
        {
            get
            {
                return idKorisnika;
            }
            private set
            {
                if (idKorisnika != value)
                {
                    idKorisnika = value;
                }
            }
        }
        public string Ime
        {
            get
            {
                return ime;
            }
            private set
            {
                if (ime != value)
                {
                    ime = value;
                }
            }
        }
        public string Prezime
        {
            get
            {
                return prezime;
            }
            private set
            {
                if (prezime != value)
                {
                    prezime = value;
                }
            }
        }
        public string Email
        {
            get
            {
                return email;
            }
            private set
            {
                if (email != value)
                {
                    email = value;
                }
            }
        }
        public string Lozinka
        {
            get
            {
                return lozinka;
            }
            private set
            {
                if (lozinka != value)
                {
                    lozinka = value;
                }
            }
        }
        public int PotvrdenKorisnik
        {
            get
            {
                return potvrdenKorisnik;
            }
            private set
            {
                if (potvrdenKorisnik != value)
                {
                    potvrdenKorisnik = value;
                }
            }
        }
        public int IdTipaKorisnika
        {
            get
            {
                return idTipaKorisnika;
            }
            private set
            {
                if (idTipaKorisnika != value)
                {
                    idTipaKorisnika = value;
                }
            }
        }
        #endregion

        #region Methods
        /// Sprema vrijednosti objekta u bazu podataka.
        public bool Spremi()
        {
            List<Korisnik> listaProvjereKOrisnika = new List<Korisnik>();
            listaProvjereKOrisnika = DohvatiSveKorisnike();
            bool postojiKorisnik = listaProvjereKOrisnika.Any(korisnik => korisnik.Email == this.Email);

            if (postojiKorisnik)
            {
                return false;
            }
            else
            {
                string sqlUpit = "";

                sqlUpit = " INSERT INTO Users  (mail , lozinka , ime , prezime , isVerified, idUserType ) VALUES( '" + Email + "', '" + Lozinka + "' , '" + Ime + "' , '" + Prezime + "' , 0, 2 ) ";
                DB.Instance.IzvrsiUpit(sqlUpit);

                return true;
            }


        }
        public bool Provjeri()
        {

            string mail = this.email;
            string lozinka = this.lozinka;
            List<Korisnik> listaProvjereKorisnika = new List<Korisnik>();
            listaProvjereKorisnika = DohvatiKorisnike();
            bool postojiKorisnik = listaProvjereKorisnika.Any(korisnik => korisnik.Email == mail && korisnik.Lozinka == lozinka);

            if (postojiKorisnik)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

     
        public int Obrisi()
        {
            string sqlDelete = "DELETE FROM Users WHERE idUser = " + Id;
            return DB.Instance.IzvrsiUpit(sqlDelete);
        }
       
        public static List<Korisnik> DohvatiKorisnike()//TRENUTNI UPIT DOHVACA SAMO POTVRDENE
        {
            List<Korisnik> lista = new List<Korisnik>();
            
            string sqlUpit = "SELECT * FROM Users WHERE isVerified=1";

            DbDataReader dr = DB.Instance.DohvatiDataReader(sqlUpit);
   
            while (dr.Read())
            {
                Korisnik korisnik = new Korisnik(dr);
                lista.Add(korisnik);
            }
            dr.Close();
            
            return lista;
        }
        public static List<Korisnik> DohvatiSveKorisnike()
        {
            List<Korisnik> lista = new List<Korisnik>();
            string sqlUpit = "SELECT * FROM Users";
            DbDataReader dr = DB.Instance.DohvatiDataReader(sqlUpit);
            while (dr.Read())
            {
                Korisnik korisnik = new Korisnik(dr);
                lista.Add(korisnik);
            }
            dr.Close();
            return lista;
        }
        public static Korisnik DohvatiKorisnika(string mail)
        {
            Korisnik dohvaceniKorisnik = new Korisnik();
            string sqlUpit = "SELECT * FROM Users WHERE mail = '" + mail + "'";
            


            List<Korisnik> lista = new List<Korisnik>();
            
            DbDataReader dr = DB.Instance.DohvatiDataReader(sqlUpit);
            while (dr.Read())
            {
                Korisnik korisnik = new Korisnik(dr);
                lista.Add(korisnik);
            }
            dr.Close();

            dohvaceniKorisnik = lista.First();
            return dohvaceniKorisnik;
        }

        #endregion

    }
}
