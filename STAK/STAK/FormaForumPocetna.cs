﻿using System;
using System.Windows.Forms;

namespace STAK
{
    public partial class FormaForumPocetna : Form
    {
        public FormaForumPocetna()
        {
            InitializeComponent();
        }

        private void zatvoriForum_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlavnaForma forma = new GlavnaForma();
            forma.Show();
        }
    }
}
