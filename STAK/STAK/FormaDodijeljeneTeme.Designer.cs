﻿namespace STAK
{
    partial class FormaDodijeljeneTeme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PovratakNaGlavniIzbornik = new System.Windows.Forms.Button();
            this.PregledDodijeljenihTema = new System.Windows.Forms.Label();
            this._17033_DBDataSet2 = new STAK._17033_DBDataSet2();
            this.pregledTemaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pregledTemaTableAdapter = new STAK._17033_DBDataSet2TableAdapters.PregledTemaTableAdapter();
            this.pregledTemaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._17033_DBDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pregledTemaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pregledTemaDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // PovratakNaGlavniIzbornik
            // 
            this.PovratakNaGlavniIzbornik.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PovratakNaGlavniIzbornik.Location = new System.Drawing.Point(58, 30);
            this.PovratakNaGlavniIzbornik.Margin = new System.Windows.Forms.Padding(2);
            this.PovratakNaGlavniIzbornik.Name = "PovratakNaGlavniIzbornik";
            this.PovratakNaGlavniIzbornik.Size = new System.Drawing.Size(99, 66);
            this.PovratakNaGlavniIzbornik.TabIndex = 8;
            this.PovratakNaGlavniIzbornik.Text = "POVRATAK NA GLAVNI IZBORNIK";
            this.PovratakNaGlavniIzbornik.UseVisualStyleBackColor = true;
            this.PovratakNaGlavniIzbornik.Click += new System.EventHandler(this.PovratakNaGlavniIzbornik_Click);
            // 
            // PregledDodijeljenihTema
            // 
            this.PregledDodijeljenihTema.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PregledDodijeljenihTema.AutoSize = true;
            this.PregledDodijeljenihTema.Location = new System.Drawing.Point(55, 128);
            this.PregledDodijeljenihTema.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PregledDodijeljenihTema.Name = "PregledDodijeljenihTema";
            this.PregledDodijeljenihTema.Size = new System.Drawing.Size(171, 17);
            this.PregledDodijeljenihTema.TabIndex = 10;
            this.PregledDodijeljenihTema.Text = "Pregled dodijeljenih tema:";
            // 
            // _17033_DBDataSet2
            // 
            this._17033_DBDataSet2.DataSetName = "_17033_DBDataSet2";
            this._17033_DBDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pregledTemaBindingSource
            // 
            this.pregledTemaBindingSource.DataMember = "PregledTema";
            this.pregledTemaBindingSource.DataSource = this._17033_DBDataSet2;
            // 
            // pregledTemaTableAdapter
            // 
            this.pregledTemaTableAdapter.ClearBeforeFill = true;
            // 
            // pregledTemaDataGridView
            // 
            this.pregledTemaDataGridView.AllowUserToAddRows = false;
            this.pregledTemaDataGridView.AllowUserToDeleteRows = false;
            this.pregledTemaDataGridView.AutoGenerateColumns = false;
            this.pregledTemaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pregledTemaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.pregledTemaDataGridView.DataSource = this.pregledTemaBindingSource;
            this.pregledTemaDataGridView.Location = new System.Drawing.Point(26, 162);
            this.pregledTemaDataGridView.Name = "pregledTemaDataGridView";
            this.pregledTemaDataGridView.ReadOnly = true;
            this.pregledTemaDataGridView.RowTemplate.Height = 24;
            this.pregledTemaDataGridView.Size = new System.Drawing.Size(886, 357);
            this.pregledTemaDataGridView.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "idProject";
            this.dataGridViewTextBoxColumn1.HeaderText = "idProject";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Naslov";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "description";
            this.dataGridViewTextBoxColumn3.HeaderText = "Opis";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "isFinished";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Završeno";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "isLectured";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Lektorirano";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "feedback";
            this.dataGridViewTextBoxColumn4.HeaderText = "Feedback";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ime";
            this.dataGridViewTextBoxColumn5.HeaderText = "Ime";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "prezime";
            this.dataGridViewTextBoxColumn6.HeaderText = "Prezime";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // FormaDodijeljeneTeme
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.pregledTemaDataGridView);
            this.Controls.Add(this.PregledDodijeljenihTema);
            this.Controls.Add(this.PovratakNaGlavniIzbornik);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormaDodijeljeneTeme";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormaDodijeljeneTeme";
            this.Load += new System.EventHandler(this.FormaDodijeljeneTeme_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FormaDodijeljeneTeme_HelpRequested);
            ((System.ComponentModel.ISupportInitialize)(this._17033_DBDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pregledTemaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pregledTemaDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PovratakNaGlavniIzbornik;
        private System.Windows.Forms.Label PregledDodijeljenihTema;
        private _17033_DBDataSet2 _17033_DBDataSet2;
        private System.Windows.Forms.BindingSource pregledTemaBindingSource;
        private _17033_DBDataSet2TableAdapters.PregledTemaTableAdapter pregledTemaTableAdapter;
        private System.Windows.Forms.DataGridView pregledTemaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    }
}