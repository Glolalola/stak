﻿namespace STAK
{
    partial class FormaPrijave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaPrijave));
            this.logo = new System.Windows.Forms.Panel();
            this.inputEmail = new System.Windows.Forms.TextBox();
            this.inputLozinka = new System.Windows.Forms.TextBox();
            this.loginGumb = new System.Windows.Forms.Button();
            this._17033_DBDataSet = new STAK._17033_DBDataSet();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter = new STAK._17033_DBDataSetTableAdapters.UsersTableAdapter();
            this.oznakaEmaila = new System.Windows.Forms.Label();
            this.oznakaLozinke = new System.Windows.Forms.Label();
            this.oznakaZaRegistraciju = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this._17033_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // logo
            // 
            this.logo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.logo.BackColor = System.Drawing.Color.Transparent;
            this.logo.BackgroundImage = global::STAK.Properties.Resources.stak_logo_790_;
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.logo.Location = new System.Drawing.Point(406, 161);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(164, 96);
            this.logo.TabIndex = 0;
            // 
            // inputEmail
            // 
            this.inputEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.inputEmail.Location = new System.Drawing.Point(347, 276);
            this.inputEmail.Name = "inputEmail";
            this.inputEmail.Size = new System.Drawing.Size(290, 20);
            this.inputEmail.TabIndex = 1;
            this.inputEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputEmail_KeyDown);
            // 
            // inputLozinka
            // 
            this.inputLozinka.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.inputLozinka.Location = new System.Drawing.Point(347, 319);
            this.inputLozinka.Name = "inputLozinka";
            this.inputLozinka.Size = new System.Drawing.Size(290, 20);
            this.inputLozinka.TabIndex = 2;
            this.inputLozinka.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputLozinka_KeyDown);
            // 
            // loginGumb
            // 
            this.loginGumb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginGumb.Location = new System.Drawing.Point(451, 345);
            this.loginGumb.Name = "loginGumb";
            this.loginGumb.Size = new System.Drawing.Size(75, 23);
            this.loginGumb.TabIndex = 3;
            this.loginGumb.Text = "LOGIN";
            this.loginGumb.UseVisualStyleBackColor = true;
            this.loginGumb.Click += new System.EventHandler(this.loginGumb_Click);
            // 
            // _17033_DBDataSet
            // 
            this._17033_DBDataSet.DataSetName = "_17033_DBDataSet";
            this._17033_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this._17033_DBDataSet;
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // oznakaEmaila
            // 
            this.oznakaEmaila.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaEmaila.AutoSize = true;
            this.oznakaEmaila.BackColor = System.Drawing.Color.Transparent;
            this.oznakaEmaila.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaEmaila.Location = new System.Drawing.Point(344, 260);
            this.oznakaEmaila.Name = "oznakaEmaila";
            this.oznakaEmaila.Size = new System.Drawing.Size(32, 13);
            this.oznakaEmaila.TabIndex = 4;
            this.oznakaEmaila.Text = "Email";
            // 
            // oznakaLozinke
            // 
            this.oznakaLozinke.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaLozinke.AutoSize = true;
            this.oznakaLozinke.BackColor = System.Drawing.Color.Transparent;
            this.oznakaLozinke.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaLozinke.Location = new System.Drawing.Point(344, 303);
            this.oznakaLozinke.Name = "oznakaLozinke";
            this.oznakaLozinke.Size = new System.Drawing.Size(44, 13);
            this.oznakaLozinke.TabIndex = 5;
            this.oznakaLozinke.Text = "Lozinka";
            // 
            // oznakaZaRegistraciju
            // 
            this.oznakaZaRegistraciju.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaZaRegistraciju.AutoSize = true;
            this.oznakaZaRegistraciju.BackColor = System.Drawing.Color.Transparent;
            this.oznakaZaRegistraciju.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaZaRegistraciju.Location = new System.Drawing.Point(455, 382);
            this.oznakaZaRegistraciju.Name = "oznakaZaRegistraciju";
            this.oznakaZaRegistraciju.Size = new System.Drawing.Size(67, 13);
            this.oznakaZaRegistraciju.TabIndex = 6;
            this.oznakaZaRegistraciju.Text = "Registriraj se";
            this.oznakaZaRegistraciju.Click += new System.EventHandler(this.oznakaZaRegistraciju_Click);
            // 
            // FormaPrijave
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(980, 557);
            this.Controls.Add(this.oznakaZaRegistraciju);
            this.Controls.Add(this.oznakaLozinke);
            this.Controls.Add(this.oznakaEmaila);
            this.Controls.Add(this.loginGumb);
            this.Controls.Add(this.inputLozinka);
            this.Controls.Add(this.inputEmail);
            this.Controls.Add(this.logo);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "mail", true));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormaPrijave";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prijava";
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FormaPrijave_HelpRequested);
            ((System.ComponentModel.ISupportInitialize)(this._17033_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel logo;
        private System.Windows.Forms.TextBox inputEmail;
        private System.Windows.Forms.TextBox inputLozinka;
        private System.Windows.Forms.Button loginGumb;
        private _17033_DBDataSet _17033_DBDataSet;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private _17033_DBDataSetTableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.Label oznakaEmaila;
        private System.Windows.Forms.Label oznakaLozinke;
        private System.Windows.Forms.Label oznakaZaRegistraciju;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}

