﻿using System;
using System.Windows.Forms;


namespace STAK
{
    public partial class FormaUrednickaAdministracija : Form
    {
        public FormaUrednickaAdministracija()
        {
            InitializeComponent();
        }
        private void UrednickaAdministracija_Load(object sender, EventArgs e)
        {
        }
        private void PovratakNaGlavniIzbornik_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlavnaForma GlavnaForma = new GlavnaForma();
            GlavnaForma.Show();
        }
        private void OdaberiTablicu_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*ForumPost-1Users-2Project-3UserProject-4Attendance-5Event-8UserType-9 IZ comba */
            //tablica=OdaberiTablicu.GetItemText(OdaberiTablicu.SelectedItem);
        }
        private void DodajUTablicu_Click(object sender, EventArgs e)
        {
        }
        private void AzurirajUTablici_Click(object sender, EventArgs e)
        {

        }
        private void IzbrisiIzTablice_Click(object sender, EventArgs e)
        {
        }
        private void FormaUrednickaAdministracija_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"https://github.com/foivz/r17033/wiki/Korisni%C4%8Dka-dokumentacija", "user-content-urednicka-administracija");
        }
    }
}
