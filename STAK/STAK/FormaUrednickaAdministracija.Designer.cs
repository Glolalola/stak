﻿namespace STAK
{
    partial class FormaUrednickaAdministracija
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaUrednickaAdministracija));
            this.PrikazPodatakaIzTablice = new System.Windows.Forms.DataGridView();
            this.OdaberiTablicu = new System.Windows.Forms.ComboBox();
            this.PovratakNaGlavniIzbornik = new System.Windows.Forms.Button();
            this.DodajUTablicu = new System.Windows.Forms.Button();
            this.AzurirajUTablici = new System.Windows.Forms.Button();
            this.IzbrisiIzTablice = new System.Windows.Forms.Button();
            this.OdabirTablice = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PrikazPodatakaIzTablice)).BeginInit();
            this.SuspendLayout();
            // 
            // PrikazPodatakaIzTablice
            // 
            this.PrikazPodatakaIzTablice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PrikazPodatakaIzTablice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PrikazPodatakaIzTablice.Location = new System.Drawing.Point(30, 251);
            this.PrikazPodatakaIzTablice.Margin = new System.Windows.Forms.Padding(2);
            this.PrikazPodatakaIzTablice.Name = "PrikazPodatakaIzTablice";
            this.PrikazPodatakaIzTablice.RowTemplate.Height = 24;
            this.PrikazPodatakaIzTablice.Size = new System.Drawing.Size(917, 287);
            this.PrikazPodatakaIzTablice.TabIndex = 0;
            // 
            // OdaberiTablicu
            // 
            this.OdaberiTablicu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OdaberiTablicu.FormattingEnabled = true;
            this.OdaberiTablicu.Items.AddRange(new object[] {
            "ForumPost-1",
            "Users-2",
            "Project-3",
            "UserProject-4",
            "Attendance-5",
            "Event-6",
            "UserType-7"});
            this.OdaberiTablicu.Location = new System.Drawing.Point(30, 197);
            this.OdaberiTablicu.Margin = new System.Windows.Forms.Padding(2);
            this.OdaberiTablicu.Name = "OdaberiTablicu";
            this.OdaberiTablicu.Size = new System.Drawing.Size(169, 24);
            this.OdaberiTablicu.TabIndex = 1;
            this.OdaberiTablicu.SelectedIndexChanged += new System.EventHandler(this.OdaberiTablicu_SelectedIndexChanged);
            // 
            // PovratakNaGlavniIzbornik
            // 
            this.PovratakNaGlavniIzbornik.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PovratakNaGlavniIzbornik.Location = new System.Drawing.Point(40, 53);
            this.PovratakNaGlavniIzbornik.Margin = new System.Windows.Forms.Padding(2);
            this.PovratakNaGlavniIzbornik.Name = "PovratakNaGlavniIzbornik";
            this.PovratakNaGlavniIzbornik.Size = new System.Drawing.Size(99, 66);
            this.PovratakNaGlavniIzbornik.TabIndex = 8;
            this.PovratakNaGlavniIzbornik.Text = "POVRATAK NA GLAVNI IZBORNIK";
            this.PovratakNaGlavniIzbornik.UseVisualStyleBackColor = true;
            this.PovratakNaGlavniIzbornik.Click += new System.EventHandler(this.PovratakNaGlavniIzbornik_Click);
            // 
            // DodajUTablicu
            // 
            this.DodajUTablicu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DodajUTablicu.Location = new System.Drawing.Point(495, 203);
            this.DodajUTablicu.Margin = new System.Windows.Forms.Padding(2);
            this.DodajUTablicu.Name = "DodajUTablicu";
            this.DodajUTablicu.Size = new System.Drawing.Size(81, 19);
            this.DodajUTablicu.TabIndex = 9;
            this.DodajUTablicu.Text = "DODAJ";
            this.DodajUTablicu.UseVisualStyleBackColor = true;
            this.DodajUTablicu.Click += new System.EventHandler(this.DodajUTablicu_Click);
            // 
            // AzurirajUTablici
            // 
            this.AzurirajUTablici.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AzurirajUTablici.Location = new System.Drawing.Point(597, 203);
            this.AzurirajUTablici.Margin = new System.Windows.Forms.Padding(2);
            this.AzurirajUTablici.Name = "AzurirajUTablici";
            this.AzurirajUTablici.Size = new System.Drawing.Size(81, 19);
            this.AzurirajUTablici.TabIndex = 10;
            this.AzurirajUTablici.Text = "PROMIJENI";
            this.AzurirajUTablici.UseVisualStyleBackColor = true;
            this.AzurirajUTablici.Click += new System.EventHandler(this.AzurirajUTablici_Click);
            // 
            // IzbrisiIzTablice
            // 
            this.IzbrisiIzTablice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IzbrisiIzTablice.Location = new System.Drawing.Point(697, 202);
            this.IzbrisiIzTablice.Margin = new System.Windows.Forms.Padding(2);
            this.IzbrisiIzTablice.Name = "IzbrisiIzTablice";
            this.IzbrisiIzTablice.Size = new System.Drawing.Size(81, 19);
            this.IzbrisiIzTablice.TabIndex = 11;
            this.IzbrisiIzTablice.Text = "IZBRIŠI";
            this.IzbrisiIzTablice.UseVisualStyleBackColor = true;
            this.IzbrisiIzTablice.Click += new System.EventHandler(this.IzbrisiIzTablice_Click);
            // 
            // OdabirTablice
            // 
            this.OdabirTablice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OdabirTablice.AutoSize = true;
            this.OdabirTablice.BackColor = System.Drawing.Color.Transparent;
            this.OdabirTablice.ForeColor = System.Drawing.Color.White;
            this.OdabirTablice.Location = new System.Drawing.Point(37, 162);
            this.OdabirTablice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.OdabirTablice.Name = "OdabirTablice";
            this.OdabirTablice.Size = new System.Drawing.Size(124, 17);
            this.OdabirTablice.TabIndex = 12;
            this.OdabirTablice.Text = "Odaberite tablicu: ";
            // 
            // FormaUrednickaAdministracija
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.OdabirTablice);
            this.Controls.Add(this.IzbrisiIzTablice);
            this.Controls.Add(this.AzurirajUTablici);
            this.Controls.Add(this.DodajUTablicu);
            this.Controls.Add(this.PovratakNaGlavniIzbornik);
            this.Controls.Add(this.OdaberiTablicu);
            this.Controls.Add(this.PrikazPodatakaIzTablice);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormaUrednickaAdministracija";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UrednickaAdministracija";
            this.Load += new System.EventHandler(this.UrednickaAdministracija_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FormaUrednickaAdministracija_HelpRequested);
            ((System.ComponentModel.ISupportInitialize)(this.PrikazPodatakaIzTablice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView PrikazPodatakaIzTablice;
        private System.Windows.Forms.ComboBox OdaberiTablicu;
        private System.Windows.Forms.Button PovratakNaGlavniIzbornik;
        private System.Windows.Forms.Button DodajUTablicu;
        private System.Windows.Forms.Button AzurirajUTablici;
        private System.Windows.Forms.Button IzbrisiIzTablice;
        private System.Windows.Forms.Label OdabirTablice;
    }
}