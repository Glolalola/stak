﻿using System.Data.SqlClient;
using System.Data.Common;

namespace STAK
{
    public class DB
    {

        private DB()
        {
            ConnectionString = @"Data Source=31.147.204.119\PISERVER,1433;Initial Catalog=17033_DB;Persist Security Info=True;User ID=17033_User;Password=EJsRauDq";
            Connection = new SqlConnection(ConnectionString); 
            Connection.Open();
        }

        ~DB()
        {
            Connection.Close();
            Connection = null;
        }



        #region Private Fields

        private static DB instance;         
        private string connectionString;     
        private SqlConnection connection; 

        #endregion

        #region Properties

        
        public static DB Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DB();
                }
                return instance;
            }
        }

        
        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
            private set
            {
                if (connectionString != value)
                {
                    connectionString = value;
                }
            }
        }
        
        public SqlConnection Connection
        {
            get
            {
                return connection;
            }
            private set
            {
                if (connection != value)
                {
                    connection = value;
                }
            }
        }

        #endregion

        #region Methods
        
        public DbDataReader DohvatiDataReader(string sqlUpit)
        {
            SqlCommand command = new SqlCommand(sqlUpit, Connection);
  
            return command.ExecuteReader();
        }

        
        public object DohvatiVrijednost(string sqlUpit)
        {
            SqlCommand command = new SqlCommand(sqlUpit, Connection);
            return command.ExecuteScalar();
        }

        
        public int IzvrsiUpit(string sqlUpit)
        {
            SqlCommand command = new SqlCommand(sqlUpit, Connection);
            return command.ExecuteNonQuery();
        }


        #endregion
    }
}

