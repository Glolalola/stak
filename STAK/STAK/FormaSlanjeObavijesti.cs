﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace STAK
{
    public partial class FormaSlanjeObavijesti : Form
    {
        private string _stakMail= "stakfoi@outlook.com";
        private string _stakLozinka = "stakpi1111";
        List<Korisnik> listaKorisnika = null;
        public FormaSlanjeObavijesti()
        {
            InitializeComponent();
        }
        private void FormaSlanjeObavijesti_Load(object sender, EventArgs e)
        {
            UcitajKorisnike();
        }
        private void PovratakNaGlavniIzbornik_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlavnaForma GlavnaForma = new GlavnaForma();
            GlavnaForma.Show();
        }
        private void PosaljiObavijest_Click(object sender, EventArgs e)
        {
            if (SadrzajObavijesti.Text != "" && NaslovObavijestiUnos.Text != "" && PrimateljiObavijesti.SelectedIndex != -1)
            {
                string OdabraniPrimatelj = PrimateljiObavijesti.SelectedItem.ToString();
                string[] ImePrezimeOdabranogPrimatelja;
                bool uspjesnoSlanje = true;
                ImePrezimeOdabranogPrimatelja = OdabraniPrimatelj.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries);
                if (ImePrezimeOdabranogPrimatelja[0] == "Svi")
                {
                    foreach (var clan in listaKorisnika)
                    {
                        if (PosaljiObavijesti(clan.Email) == false)
                        {
                            uspjesnoSlanje = false;
                        }
                    }
                    if (uspjesnoSlanje)
                    {
                        MessageBox.Show("Obavijest poslana");

                    }
                    else
                    {
                        MessageBox.Show("Doslo je do greške u slanju obavijesti svim korisnicima");
                    }
                    OcistiPoljaZaUnosNaFormi();
                }
                else
                {
                    foreach (var clan in listaKorisnika)
                    {
                        if (clan.Ime == ImePrezimeOdabranogPrimatelja[0] && clan.Prezime == ImePrezimeOdabranogPrimatelja[1])
                        {
                            if (PosaljiObavijesti(clan.Email) == true)
                            {
                                MessageBox.Show("Obavijest poslana clanu "+clan.Ime +" " + clan.Prezime);
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Doslo je do greske u slanju obavijesti korisniku " + clan.Ime + " " + clan.Prezime);
                            }
                        }
                    }

                    OcistiPoljaZaUnosNaFormi();
                }
            }
            else
            {
                MessageBox.Show("Nisu ispravno uneseni svi podaci za slanje obavijesti :( ");
            }
        }
        private bool PosaljiObavijesti(string emailPrimatelja) 
        {            
            try
            {
                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.Host = "outlook.com";
                    smtp.UseDefaultCredentials = false;
                    NetworkCredential netcred = new NetworkCredential(_stakMail, _stakLozinka);
                    smtp.Credentials = netcred;
                    smtp.EnableSsl = true;

                    using (MailMessage msg = new MailMessage(_stakMail, emailPrimatelja))
                    {
                        msg.Subject = NaslovObavijestiUnos.Text;
                        StringBuilder sb = new StringBuilder();
                        sb.Append(SadrzajObavijesti.Text);
                        msg.Body = sb.ToString();
                        msg.IsBodyHtml = false;
                        smtp.Send(msg);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Doslo je do pogreske u slanju obavijesti"+Environment.NewLine+"Poruka greske: "+ex.Message );
                return false;
            }
        }
        private void OcistiPoljaZaUnosNaFormi()
        {
            SadrzajObavijesti.Text = "";
            NaslovObavijestiUnos.Text = "";
        }

        private void UcitajKorisnike()
        {
            listaKorisnika = Korisnik.DohvatiKorisnike();
            PrimateljiObavijesti.Items.Add("Svi");
            foreach (var verificiraniKorisnik in listaKorisnika)
            {
                PrimateljiObavijesti.Items.Add(verificiraniKorisnik.Ime + " " + verificiraniKorisnik.Prezime);
            }
        }
        private void FormaSlanjeObavijesti_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"https://github.com/foivz/r17033/wiki/Korisni%C4%8Dka-dokumentacija", "user-content-slanje-obavijesti");

        }
    }
}
