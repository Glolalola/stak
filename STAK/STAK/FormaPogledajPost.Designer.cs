﻿namespace STAK
{
    partial class FormaPogledajPost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaPogledajPost));
            this.naslovPosta = new System.Windows.Forms.Label();
            this.sadrzajPosta = new System.Windows.Forms.RichTextBox();
            this.komentari = new System.Windows.Forms.ListBox();
            this.dodajKomentar = new System.Windows.Forms.Button();
            this.zatvoriPost = new System.Windows.Forms.Button();
            this.korisnikPost = new System.Windows.Forms.Label();
            this.datumPosta = new System.Windows.Forms.Label();
            this.sadrzajPost = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // naslovPosta
            // 
            this.naslovPosta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.naslovPosta.AutoSize = true;
            this.naslovPosta.BackColor = System.Drawing.Color.Transparent;
            this.naslovPosta.Font = new System.Drawing.Font("Roboto Condensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.naslovPosta.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.naslovPosta.Location = new System.Drawing.Point(229, 72);
            this.naslovPosta.Name = "naslovPosta";
            this.naslovPosta.Size = new System.Drawing.Size(182, 38);
            this.naslovPosta.TabIndex = 0;
            this.naslovPosta.Text = "Naslov posta";
            // 
            // sadrzajPosta
            // 
            this.sadrzajPosta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sadrzajPosta.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.sadrzajPosta.Location = new System.Drawing.Point(236, 135);
            this.sadrzajPosta.Name = "sadrzajPosta";
            this.sadrzajPosta.Size = new System.Drawing.Size(519, 148);
            this.sadrzajPosta.TabIndex = 1;
            this.sadrzajPosta.Text = "";
            // 
            // komentari
            // 
            this.komentari.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.komentari.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.komentari.FormattingEnabled = true;
            this.komentari.Location = new System.Drawing.Point(236, 303);
            this.komentari.Name = "komentari";
            this.komentari.Size = new System.Drawing.Size(268, 186);
            this.komentari.TabIndex = 2;
            // 
            // dodajKomentar
            // 
            this.dodajKomentar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dodajKomentar.Location = new System.Drawing.Point(567, 318);
            this.dodajKomentar.Name = "dodajKomentar";
            this.dodajKomentar.Size = new System.Drawing.Size(127, 36);
            this.dodajKomentar.TabIndex = 3;
            this.dodajKomentar.Text = "Dodaj komentar";
            this.dodajKomentar.UseVisualStyleBackColor = true;
            // 
            // zatvoriPost
            // 
            this.zatvoriPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.zatvoriPost.Location = new System.Drawing.Point(567, 370);
            this.zatvoriPost.Name = "zatvoriPost";
            this.zatvoriPost.Size = new System.Drawing.Size(127, 39);
            this.zatvoriPost.TabIndex = 4;
            this.zatvoriPost.Text = "Zatvori post";
            this.zatvoriPost.UseVisualStyleBackColor = true;
            // 
            // korisnikPost
            // 
            this.korisnikPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.korisnikPost.AutoSize = true;
            this.korisnikPost.BackColor = System.Drawing.Color.Transparent;
            this.korisnikPost.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.korisnikPost.Location = new System.Drawing.Point(598, 119);
            this.korisnikPost.Name = "korisnikPost";
            this.korisnikPost.Size = new System.Drawing.Size(44, 13);
            this.korisnikPost.TabIndex = 5;
            this.korisnikPost.Text = "Korsinik";
            // 
            // datumPosta
            // 
            this.datumPosta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.datumPosta.AutoSize = true;
            this.datumPosta.BackColor = System.Drawing.Color.Transparent;
            this.datumPosta.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.datumPosta.Location = new System.Drawing.Point(675, 119);
            this.datumPosta.Name = "datumPosta";
            this.datumPosta.Size = new System.Drawing.Size(36, 13);
            this.datumPosta.TabIndex = 6;
            this.datumPosta.Text = "datum";
            // 
            // sadrzajPost
            // 
            this.sadrzajPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sadrzajPost.AutoSize = true;
            this.sadrzajPost.BackColor = System.Drawing.Color.Transparent;
            this.sadrzajPost.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sadrzajPost.Location = new System.Drawing.Point(236, 119);
            this.sadrzajPost.Name = "sadrzajPost";
            this.sadrzajPost.Size = new System.Drawing.Size(71, 13);
            this.sadrzajPost.TabIndex = 7;
            this.sadrzajPost.Text = "Sadržaj posta";
            // 
            // FormaPogledajPost
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.sadrzajPost);
            this.Controls.Add(this.datumPosta);
            this.Controls.Add(this.korisnikPost);
            this.Controls.Add(this.zatvoriPost);
            this.Controls.Add(this.dodajKomentar);
            this.Controls.Add(this.komentari);
            this.Controls.Add(this.sadrzajPosta);
            this.Controls.Add(this.naslovPosta);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormaPogledajPost";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormaPogledajPost";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label naslovPosta;
        private System.Windows.Forms.RichTextBox sadrzajPosta;
        private System.Windows.Forms.ListBox komentari;
        private System.Windows.Forms.Button dodajKomentar;
        private System.Windows.Forms.Button zatvoriPost;
        private System.Windows.Forms.Label korisnikPost;
        private System.Windows.Forms.Label datumPosta;
        private System.Windows.Forms.Label sadrzajPost;
    }
}