﻿namespace STAK
{
    partial class FormaForumPocetna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaForumPocetna));
            this.label1 = new System.Windows.Forms.Label();
            this.sviPostovi = new System.Windows.Forms.ListBox();
            this.pogledajPost = new System.Windows.Forms.Button();
            this.noviPost = new System.Windows.Forms.Button();
            this.zatvoriForum = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Roboto Condensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(154, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "FORUM";
            // 
            // sviPostovi
            // 
            this.sviPostovi.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sviPostovi.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.sviPostovi.FormattingEnabled = true;
            this.sviPostovi.Location = new System.Drawing.Point(162, 186);
            this.sviPostovi.Name = "sviPostovi";
            this.sviPostovi.Size = new System.Drawing.Size(491, 316);
            this.sviPostovi.TabIndex = 1;
            // 
            // pogledajPost
            // 
            this.pogledajPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pogledajPost.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pogledajPost.FlatAppearance.BorderSize = 0;
            this.pogledajPost.Location = new System.Drawing.Point(693, 186);
            this.pogledajPost.Name = "pogledajPost";
            this.pogledajPost.Size = new System.Drawing.Size(138, 23);
            this.pogledajPost.TabIndex = 2;
            this.pogledajPost.Text = "Pogledaj post";
            this.pogledajPost.UseVisualStyleBackColor = false;
            // 
            // noviPost
            // 
            this.noviPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.noviPost.Location = new System.Drawing.Point(693, 237);
            this.noviPost.Name = "noviPost";
            this.noviPost.Size = new System.Drawing.Size(138, 26);
            this.noviPost.TabIndex = 3;
            this.noviPost.Text = "Novi post";
            this.noviPost.UseVisualStyleBackColor = true;
            // 
            // zatvoriForum
            // 
            this.zatvoriForum.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.zatvoriForum.Location = new System.Drawing.Point(161, 58);
            this.zatvoriForum.Name = "zatvoriForum";
            this.zatvoriForum.Size = new System.Drawing.Size(137, 23);
            this.zatvoriForum.TabIndex = 4;
            this.zatvoriForum.Text = "Zatvori forum";
            this.zatvoriForum.UseVisualStyleBackColor = true;
            this.zatvoriForum.Click += new System.EventHandler(this.zatvoriForum_Click);
            // 
            // FormaForumPocetna
            // 
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.zatvoriForum);
            this.Controls.Add(this.noviPost);
            this.Controls.Add(this.pogledajPost);
            this.Controls.Add(this.sviPostovi);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormaForumPocetna";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label naslov;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox sviPostovi;
        private System.Windows.Forms.Button pogledajPost;
        private System.Windows.Forms.Button noviPost;
        private System.Windows.Forms.Button zatvoriForum;
    }
}