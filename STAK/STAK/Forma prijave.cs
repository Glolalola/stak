﻿using System;
using System.Windows.Forms;

namespace STAK
{
    public partial class FormaPrijave : Form
    {
        
        public FormaPrijave()
        {
            InitializeComponent();
            inputLozinka.PasswordChar = '*';
        }

        private void loginGumb_Click(object sender, EventArgs e)
        {
            prijava();

        }

     

        private void oznakaZaRegistraciju_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormaRegistracije registracijskaForma = new FormaRegistracije();
            registracijskaForma.Show();
        }

        

        private void inputEmail_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                prijava();

            }
        }
        public void prijava()
        {

            if (inputEmail.Text == "" || inputLozinka.Text == "")
            {
                MessageBox.Show("Morate upisati email i lozinku!");
                return;
            }
            try
            {
                //kreiraj korisnika

                Korisnik uneseniKorisnik = new Korisnik(inputEmail.Text.ToString(), inputLozinka.Text.ToString());
                
                
                if (uneseniKorisnik.Provjeri())
                {
                    MessageBox.Show("Uspješna prijava!");

                    //dohvat ostalih podataka o korisniku iz baze

                    uneseniKorisnik = Korisnik.DohvatiKorisnika(inputEmail.Text.ToString());

                    //spremanje korisnika u "sesiju"

                    PrijavljeniKorisnik.idKorisnika = uneseniKorisnik.Id;
                    PrijavljeniKorisnik.email = uneseniKorisnik.Email;
                    PrijavljeniKorisnik.ime = uneseniKorisnik.Ime;
                    PrijavljeniKorisnik.prezime = uneseniKorisnik.Prezime;                 
                    PrijavljeniKorisnik.idTipaKorisnika = uneseniKorisnik.IdTipaKorisnika;
                    
                    this.Hide();
                    GlavnaForma fm = new GlavnaForma();
                    fm.Show();
                }
                else
                {
                    MessageBox.Show("Neuspješna prijava!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void inputLozinka_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                prijava();

            }
        }

        private void FormaPrijave_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"https://github.com/foivz/r17033/wiki/Korisni%C4%8Dka-dokumentacija", "prijava");

        }
    }
}
