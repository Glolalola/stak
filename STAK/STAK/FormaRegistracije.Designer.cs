﻿namespace STAK
{
    partial class FormaRegistracije
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaRegistracije));
            this.inputEmail = new System.Windows.Forms.TextBox();
            this.inputLozinka = new System.Windows.Forms.TextBox();
            this.inputPonovljeneLozinke = new System.Windows.Forms.TextBox();
            this.gumbRegistracije = new System.Windows.Forms.Button();
            this.oznakaGreske = new System.Windows.Forms.Label();
            this.oznakaEmaila = new System.Windows.Forms.Label();
            this.oznakaLozinke = new System.Windows.Forms.Label();
            this.inputPrezime = new System.Windows.Forms.TextBox();
            this.inputIme = new System.Windows.Forms.TextBox();
            this.oznakaImena = new System.Windows.Forms.Label();
            this.oznakaPrezimena = new System.Windows.Forms.Label();
            this.oznakaPonovljeneLozinke = new System.Windows.Forms.Label();
            this.odustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inputEmail
            // 
            this.inputEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.inputEmail.Location = new System.Drawing.Point(435, 233);
            this.inputEmail.Name = "inputEmail";
            this.inputEmail.Size = new System.Drawing.Size(160, 20);
            this.inputEmail.TabIndex = 2;
            // 
            // inputLozinka
            // 
            this.inputLozinka.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.inputLozinka.Location = new System.Drawing.Point(435, 274);
            this.inputLozinka.Name = "inputLozinka";
            this.inputLozinka.Size = new System.Drawing.Size(160, 20);
            this.inputLozinka.TabIndex = 3;
            // 
            // inputPonovljeneLozinke
            // 
            this.inputPonovljeneLozinke.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.inputPonovljeneLozinke.Location = new System.Drawing.Point(435, 315);
            this.inputPonovljeneLozinke.Name = "inputPonovljeneLozinke";
            this.inputPonovljeneLozinke.Size = new System.Drawing.Size(160, 20);
            this.inputPonovljeneLozinke.TabIndex = 4;
            // 
            // gumbRegistracije
            // 
            this.gumbRegistracije.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gumbRegistracije.Location = new System.Drawing.Point(435, 348);
            this.gumbRegistracije.Name = "gumbRegistracije";
            this.gumbRegistracije.Size = new System.Drawing.Size(160, 23);
            this.gumbRegistracije.TabIndex = 5;
            this.gumbRegistracije.Text = "Registriraj se";
            this.gumbRegistracije.UseVisualStyleBackColor = true;
            this.gumbRegistracije.Click += new System.EventHandler(this.gumbRegistracije_Click);
            // 
            // oznakaGreske
            // 
            this.oznakaGreske.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaGreske.AutoSize = true;
            this.oznakaGreske.BackColor = System.Drawing.Color.Transparent;
            this.oznakaGreske.ForeColor = System.Drawing.Color.Red;
            this.oznakaGreske.Location = new System.Drawing.Point(580, 299);
            this.oznakaGreske.Name = "oznakaGreske";
            this.oznakaGreske.Size = new System.Drawing.Size(75, 13);
            this.oznakaGreske.TabIndex = 12;
            this.oznakaGreske.Text = "greska lozinke";
            // 
            // oznakaEmaila
            // 
            this.oznakaEmaila.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaEmaila.AutoSize = true;
            this.oznakaEmaila.BackColor = System.Drawing.Color.Transparent;
            this.oznakaEmaila.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaEmaila.Location = new System.Drawing.Point(394, 233);
            this.oznakaEmaila.Name = "oznakaEmaila";
            this.oznakaEmaila.Size = new System.Drawing.Size(35, 13);
            this.oznakaEmaila.TabIndex = 9;
            this.oznakaEmaila.Text = "Email:";
            // 
            // oznakaLozinke
            // 
            this.oznakaLozinke.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaLozinke.AutoSize = true;
            this.oznakaLozinke.BackColor = System.Drawing.Color.Transparent;
            this.oznakaLozinke.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaLozinke.Location = new System.Drawing.Point(382, 274);
            this.oznakaLozinke.Name = "oznakaLozinke";
            this.oznakaLozinke.Size = new System.Drawing.Size(47, 13);
            this.oznakaLozinke.TabIndex = 10;
            this.oznakaLozinke.Text = "Lozinka:";
            // 
            // inputPrezime
            // 
            this.inputPrezime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.inputPrezime.Location = new System.Drawing.Point(435, 192);
            this.inputPrezime.Name = "inputPrezime";
            this.inputPrezime.Size = new System.Drawing.Size(160, 20);
            this.inputPrezime.TabIndex = 1;
            // 
            // inputIme
            // 
            this.inputIme.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.inputIme.Location = new System.Drawing.Point(435, 151);
            this.inputIme.Name = "inputIme";
            this.inputIme.Size = new System.Drawing.Size(160, 20);
            this.inputIme.TabIndex = 0;
            // 
            // oznakaImena
            // 
            this.oznakaImena.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaImena.AutoSize = true;
            this.oznakaImena.BackColor = System.Drawing.Color.Transparent;
            this.oznakaImena.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaImena.Location = new System.Drawing.Point(402, 151);
            this.oznakaImena.Name = "oznakaImena";
            this.oznakaImena.Size = new System.Drawing.Size(27, 13);
            this.oznakaImena.TabIndex = 7;
            this.oznakaImena.Text = "Ime:";
            // 
            // oznakaPrezimena
            // 
            this.oznakaPrezimena.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaPrezimena.AutoSize = true;
            this.oznakaPrezimena.BackColor = System.Drawing.Color.Transparent;
            this.oznakaPrezimena.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaPrezimena.Location = new System.Drawing.Point(382, 192);
            this.oznakaPrezimena.Name = "oznakaPrezimena";
            this.oznakaPrezimena.Size = new System.Drawing.Size(47, 13);
            this.oznakaPrezimena.TabIndex = 8;
            this.oznakaPrezimena.Text = "Prezime:";
            // 
            // oznakaPonovljeneLozinke
            // 
            this.oznakaPonovljeneLozinke.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaPonovljeneLozinke.AutoSize = true;
            this.oznakaPonovljeneLozinke.BackColor = System.Drawing.Color.Transparent;
            this.oznakaPonovljeneLozinke.ForeColor = System.Drawing.SystemColors.Window;
            this.oznakaPonovljeneLozinke.Location = new System.Drawing.Point(330, 315);
            this.oznakaPonovljeneLozinke.Name = "oznakaPonovljeneLozinke";
            this.oznakaPonovljeneLozinke.Size = new System.Drawing.Size(99, 13);
            this.oznakaPonovljeneLozinke.TabIndex = 11;
            this.oznakaPonovljeneLozinke.Text = "Ponovljena lozinka:";
            // 
            // odustani
            // 
            this.odustani.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.odustani.Location = new System.Drawing.Point(435, 386);
            this.odustani.Name = "odustani";
            this.odustani.Size = new System.Drawing.Size(160, 23);
            this.odustani.TabIndex = 6;
            this.odustani.Text = "Odustani";
            this.odustani.UseVisualStyleBackColor = true;
            this.odustani.Click += new System.EventHandler(this.odustani_Click);
            // 
            // FormaRegistracije
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.odustani);
            this.Controls.Add(this.oznakaPonovljeneLozinke);
            this.Controls.Add(this.oznakaPrezimena);
            this.Controls.Add(this.oznakaImena);
            this.Controls.Add(this.inputIme);
            this.Controls.Add(this.inputPrezime);
            this.Controls.Add(this.oznakaLozinke);
            this.Controls.Add(this.oznakaEmaila);
            this.Controls.Add(this.oznakaGreske);
            this.Controls.Add(this.gumbRegistracije);
            this.Controls.Add(this.inputPonovljeneLozinke);
            this.Controls.Add(this.inputLozinka);
            this.Controls.Add(this.inputEmail);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormaRegistracije";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registracija";
            this.Load += new System.EventHandler(this.RegistrationForm_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FormaRegistracije_HelpRequested);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox inputEmail;
        private System.Windows.Forms.TextBox inputLozinka;
        private System.Windows.Forms.TextBox inputPonovljeneLozinke;
        private System.Windows.Forms.Button gumbRegistracije;
        private System.Windows.Forms.Label oznakaGreske;
        private System.Windows.Forms.Label oznakaEmaila;
        private System.Windows.Forms.Label oznakaLozinke;
        private System.Windows.Forms.TextBox inputPrezime;
        private System.Windows.Forms.TextBox inputIme;
        private System.Windows.Forms.Label oznakaImena;
        private System.Windows.Forms.Label oznakaPrezimena;
        private System.Windows.Forms.Label oznakaPonovljeneLozinke;
        private System.Windows.Forms.Button odustani;
    }
}