﻿namespace STAK
{
    partial class Kalendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Kalendar));
            this.prikazKalendara = new System.Windows.Forms.MonthCalendar();
            this.imeEventa = new System.Windows.Forms.TextBox();
            this.oznakaImenaEventa = new System.Windows.Forms.Label();
            this.oznakaDatumaEventa = new System.Windows.Forms.Label();
            this.prikazOdabranogDatuma = new System.Windows.Forms.Label();
            this.oznakaVremenaPocetka = new System.Windows.Forms.Label();
            this.oznakaVremenaZavrsetka = new System.Windows.Forms.Label();
            this.oznakaOpisa = new System.Windows.Forms.Label();
            this.opisEventa = new System.Windows.Forms.TextBox();
            this.oznakaLokacijeEventa = new System.Windows.Forms.Label();
            this.lokacijaEventa = new System.Windows.Forms.TextBox();
            this.oznakaPodsjetnikaEventa = new System.Windows.Forms.Label();
            this.dodajEvent = new System.Windows.Forms.Button();
            this.podsjetnikEventa = new System.Windows.Forms.ComboBox();
            this.povratakNaGlavnuFormu = new System.Windows.Forms.Button();
            this.eventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._17033_DBDataSet1 = new STAK._17033_DBDataSet1();
            this.eventTableAdapter = new STAK._17033_DBDataSet1TableAdapters.EventTableAdapter();
            this.tableAdapterManager = new STAK._17033_DBDataSet1TableAdapters.TableAdapterManager();
            this.eventDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vrijemePocetkaEventa = new System.Windows.Forms.DateTimePicker();
            this.vrijemeZavrsetkaEventa = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._17033_DBDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // prikazKalendara
            // 
            this.prikazKalendara.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.prikazKalendara.BackColor = System.Drawing.SystemColors.MenuText;
            this.prikazKalendara.Location = new System.Drawing.Point(101, 77);
            this.prikazKalendara.Name = "prikazKalendara";
            this.prikazKalendara.TabIndex = 0;
            this.prikazKalendara.TrailingForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.prikazKalendara.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.prikazKalendara_DateChanged);
            // 
            // imeEventa
            // 
            this.imeEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imeEventa.Location = new System.Drawing.Point(411, 119);
            this.imeEventa.Name = "imeEventa";
            this.imeEventa.Size = new System.Drawing.Size(195, 20);
            this.imeEventa.TabIndex = 1;
            // 
            // oznakaImenaEventa
            // 
            this.oznakaImenaEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaImenaEventa.AutoSize = true;
            this.oznakaImenaEventa.BackColor = System.Drawing.Color.Transparent;
            this.oznakaImenaEventa.ForeColor = System.Drawing.Color.White;
            this.oznakaImenaEventa.Location = new System.Drawing.Point(368, 122);
            this.oznakaImenaEventa.Name = "oznakaImenaEventa";
            this.oznakaImenaEventa.Size = new System.Drawing.Size(37, 13);
            this.oznakaImenaEventa.TabIndex = 2;
            this.oznakaImenaEventa.Text = "Naziv:";
            // 
            // oznakaDatumaEventa
            // 
            this.oznakaDatumaEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaDatumaEventa.AutoSize = true;
            this.oznakaDatumaEventa.BackColor = System.Drawing.Color.Transparent;
            this.oznakaDatumaEventa.ForeColor = System.Drawing.Color.White;
            this.oznakaDatumaEventa.Location = new System.Drawing.Point(364, 96);
            this.oznakaDatumaEventa.Name = "oznakaDatumaEventa";
            this.oznakaDatumaEventa.Size = new System.Drawing.Size(41, 13);
            this.oznakaDatumaEventa.TabIndex = 3;
            this.oznakaDatumaEventa.Text = "Datum:";
            // 
            // prikazOdabranogDatuma
            // 
            this.prikazOdabranogDatuma.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.prikazOdabranogDatuma.AutoSize = true;
            this.prikazOdabranogDatuma.BackColor = System.Drawing.Color.Transparent;
            this.prikazOdabranogDatuma.ForeColor = System.Drawing.Color.White;
            this.prikazOdabranogDatuma.Location = new System.Drawing.Point(411, 94);
            this.prikazOdabranogDatuma.Name = "prikazOdabranogDatuma";
            this.prikazOdabranogDatuma.Size = new System.Drawing.Size(0, 13);
            this.prikazOdabranogDatuma.TabIndex = 4;
            // 
            // oznakaVremenaPocetka
            // 
            this.oznakaVremenaPocetka.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaVremenaPocetka.AutoSize = true;
            this.oznakaVremenaPocetka.BackColor = System.Drawing.Color.Transparent;
            this.oznakaVremenaPocetka.ForeColor = System.Drawing.Color.White;
            this.oznakaVremenaPocetka.Location = new System.Drawing.Point(319, 154);
            this.oznakaVremenaPocetka.Name = "oznakaVremenaPocetka";
            this.oznakaVremenaPocetka.Size = new System.Drawing.Size(86, 13);
            this.oznakaVremenaPocetka.TabIndex = 5;
            this.oznakaVremenaPocetka.Text = "Vrijeme početka:";
            // 
            // oznakaVremenaZavrsetka
            // 
            this.oznakaVremenaZavrsetka.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaVremenaZavrsetka.AutoSize = true;
            this.oznakaVremenaZavrsetka.BackColor = System.Drawing.Color.Transparent;
            this.oznakaVremenaZavrsetka.ForeColor = System.Drawing.Color.White;
            this.oznakaVremenaZavrsetka.Location = new System.Drawing.Point(312, 186);
            this.oznakaVremenaZavrsetka.Name = "oznakaVremenaZavrsetka";
            this.oznakaVremenaZavrsetka.Size = new System.Drawing.Size(93, 13);
            this.oznakaVremenaZavrsetka.TabIndex = 7;
            this.oznakaVremenaZavrsetka.Text = "Vrijeme završetka:";
            // 
            // oznakaOpisa
            // 
            this.oznakaOpisa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaOpisa.AutoSize = true;
            this.oznakaOpisa.BackColor = System.Drawing.Color.Transparent;
            this.oznakaOpisa.ForeColor = System.Drawing.Color.White;
            this.oznakaOpisa.Location = new System.Drawing.Point(652, 99);
            this.oznakaOpisa.Name = "oznakaOpisa";
            this.oznakaOpisa.Size = new System.Drawing.Size(31, 13);
            this.oznakaOpisa.TabIndex = 9;
            this.oznakaOpisa.Text = "Opis:";
            // 
            // opisEventa
            // 
            this.opisEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.opisEventa.Location = new System.Drawing.Point(689, 96);
            this.opisEventa.Multiline = true;
            this.opisEventa.Name = "opisEventa";
            this.opisEventa.Size = new System.Drawing.Size(195, 46);
            this.opisEventa.TabIndex = 10;
            // 
            // oznakaLokacijeEventa
            // 
            this.oznakaLokacijeEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaLokacijeEventa.AutoSize = true;
            this.oznakaLokacijeEventa.BackColor = System.Drawing.Color.Transparent;
            this.oznakaLokacijeEventa.ForeColor = System.Drawing.Color.White;
            this.oznakaLokacijeEventa.Location = new System.Drawing.Point(633, 157);
            this.oznakaLokacijeEventa.Name = "oznakaLokacijeEventa";
            this.oznakaLokacijeEventa.Size = new System.Drawing.Size(50, 13);
            this.oznakaLokacijeEventa.TabIndex = 11;
            this.oznakaLokacijeEventa.Text = "Lokacija:";
            // 
            // lokacijaEventa
            // 
            this.lokacijaEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lokacijaEventa.Location = new System.Drawing.Point(689, 153);
            this.lokacijaEventa.Name = "lokacijaEventa";
            this.lokacijaEventa.Size = new System.Drawing.Size(195, 20);
            this.lokacijaEventa.TabIndex = 12;
            // 
            // oznakaPodsjetnikaEventa
            // 
            this.oznakaPodsjetnikaEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.oznakaPodsjetnikaEventa.AutoSize = true;
            this.oznakaPodsjetnikaEventa.BackColor = System.Drawing.Color.Transparent;
            this.oznakaPodsjetnikaEventa.ForeColor = System.Drawing.Color.White;
            this.oznakaPodsjetnikaEventa.Location = new System.Drawing.Point(624, 189);
            this.oznakaPodsjetnikaEventa.Name = "oznakaPodsjetnikaEventa";
            this.oznakaPodsjetnikaEventa.Size = new System.Drawing.Size(59, 13);
            this.oznakaPodsjetnikaEventa.TabIndex = 13;
            this.oznakaPodsjetnikaEventa.Text = "Podsjetnik:";
            // 
            // dodajEvent
            // 
            this.dodajEvent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dodajEvent.Location = new System.Drawing.Point(689, 216);
            this.dodajEvent.Name = "dodajEvent";
            this.dodajEvent.Size = new System.Drawing.Size(195, 23);
            this.dodajEvent.TabIndex = 15;
            this.dodajEvent.Text = "Dodaj Event";
            this.dodajEvent.UseVisualStyleBackColor = true;
            this.dodajEvent.Click += new System.EventHandler(this.dodajEvent_Click);
            // 
            // podsjetnikEventa
            // 
            this.podsjetnikEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.podsjetnikEventa.FormattingEnabled = true;
            this.podsjetnikEventa.Location = new System.Drawing.Point(689, 184);
            this.podsjetnikEventa.Name = "podsjetnikEventa";
            this.podsjetnikEventa.Size = new System.Drawing.Size(195, 21);
            this.podsjetnikEventa.TabIndex = 17;
            // 
            // povratakNaGlavnuFormu
            // 
            this.povratakNaGlavnuFormu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.povratakNaGlavnuFormu.Location = new System.Drawing.Point(101, 42);
            this.povratakNaGlavnuFormu.Name = "povratakNaGlavnuFormu";
            this.povratakNaGlavnuFormu.Size = new System.Drawing.Size(199, 23);
            this.povratakNaGlavnuFormu.TabIndex = 18;
            this.povratakNaGlavnuFormu.Text = "Povratak na glavni izbornik";
            this.povratakNaGlavnuFormu.UseVisualStyleBackColor = true;
            this.povratakNaGlavnuFormu.Click += new System.EventHandler(this.povratakNaGlavnuFormu_Click);
            // 
            // eventBindingSource
            // 
            this.eventBindingSource.DataMember = "Event";
            this.eventBindingSource.DataSource = this._17033_DBDataSet1;
            // 
            // _17033_DBDataSet1
            // 
            this._17033_DBDataSet1.DataSetName = "_17033_DBDataSet1";
            this._17033_DBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // eventTableAdapter
            // 
            this.eventTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.EventTableAdapter = this.eventTableAdapter;
            this.tableAdapterManager.UpdateOrder = STAK._17033_DBDataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // eventDataGridView
            // 
            this.eventDataGridView.AllowUserToAddRows = false;
            this.eventDataGridView.AllowUserToDeleteRows = false;
            this.eventDataGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.eventDataGridView.AutoGenerateColumns = false;
            this.eventDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.eventDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.eventDataGridView.DataSource = this.eventBindingSource;
            this.eventDataGridView.Location = new System.Drawing.Point(101, 264);
            this.eventDataGridView.Name = "eventDataGridView";
            this.eventDataGridView.ReadOnly = true;
            this.eventDataGridView.Size = new System.Drawing.Size(783, 220);
            this.eventDataGridView.TabIndex = 18;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn2.HeaderText = "name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "eventDate";
            this.dataGridViewTextBoxColumn3.HeaderText = "eventDate";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "startTime";
            this.dataGridViewTextBoxColumn4.HeaderText = "startTime";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "endTime";
            this.dataGridViewTextBoxColumn5.HeaderText = "endTime";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "description";
            this.dataGridViewTextBoxColumn6.HeaderText = "description";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "location";
            this.dataGridViewTextBoxColumn7.HeaderText = "location";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "reminder";
            this.dataGridViewTextBoxColumn8.HeaderText = "reminder";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // vrijemePocetkaEventa
            // 
            this.vrijemePocetkaEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vrijemePocetkaEventa.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.vrijemePocetkaEventa.Location = new System.Drawing.Point(411, 151);
            this.vrijemePocetkaEventa.Name = "vrijemePocetkaEventa";
            this.vrijemePocetkaEventa.ShowUpDown = true;
            this.vrijemePocetkaEventa.Size = new System.Drawing.Size(195, 20);
            this.vrijemePocetkaEventa.TabIndex = 19;
            this.vrijemePocetkaEventa.Value = new System.DateTime(2017, 6, 20, 0, 0, 0, 0);
            // 
            // vrijemeZavrsetkaEventa
            // 
            this.vrijemeZavrsetkaEventa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vrijemeZavrsetkaEventa.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.vrijemeZavrsetkaEventa.Location = new System.Drawing.Point(411, 183);
            this.vrijemeZavrsetkaEventa.Name = "vrijemeZavrsetkaEventa";
            this.vrijemeZavrsetkaEventa.ShowUpDown = true;
            this.vrijemeZavrsetkaEventa.Size = new System.Drawing.Size(195, 20);
            this.vrijemeZavrsetkaEventa.TabIndex = 20;
            this.vrijemeZavrsetkaEventa.Value = new System.DateTime(2017, 6, 20, 0, 0, 0, 0);
            // 
            // Kalendar
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.vrijemeZavrsetkaEventa);
            this.Controls.Add(this.vrijemePocetkaEventa);
            this.Controls.Add(this.eventDataGridView);
            this.Controls.Add(this.povratakNaGlavnuFormu);
            this.Controls.Add(this.podsjetnikEventa);
            this.Controls.Add(this.dodajEvent);
            this.Controls.Add(this.oznakaPodsjetnikaEventa);
            this.Controls.Add(this.lokacijaEventa);
            this.Controls.Add(this.oznakaLokacijeEventa);
            this.Controls.Add(this.opisEventa);
            this.Controls.Add(this.oznakaOpisa);
            this.Controls.Add(this.oznakaVremenaZavrsetka);
            this.Controls.Add(this.oznakaVremenaPocetka);
            this.Controls.Add(this.prikazOdabranogDatuma);
            this.Controls.Add(this.oznakaDatumaEventa);
            this.Controls.Add(this.oznakaImenaEventa);
            this.Controls.Add(this.imeEventa);
            this.Controls.Add(this.prikazKalendara);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Kalendar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kalendar";
            this.Load += new System.EventHandler(this.Kalendar_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.Kalendar_HelpRequested);
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._17033_DBDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar prikazKalendara;
        private System.Windows.Forms.TextBox imeEventa;
        private System.Windows.Forms.Label oznakaImenaEventa;
        private System.Windows.Forms.Label oznakaDatumaEventa;
        private System.Windows.Forms.Label prikazOdabranogDatuma;
        private System.Windows.Forms.Label oznakaVremenaPocetka;
        private System.Windows.Forms.Label oznakaVremenaZavrsetka;
        private System.Windows.Forms.Label oznakaOpisa;
        private System.Windows.Forms.TextBox opisEventa;
        private System.Windows.Forms.Label oznakaLokacijeEventa;
        private System.Windows.Forms.TextBox lokacijaEventa;
        private System.Windows.Forms.Label oznakaPodsjetnikaEventa;
        private System.Windows.Forms.Button dodajEvent;
        private _17033_DBDataSet1 _17033_DBDataSet1;
        private System.Windows.Forms.BindingSource eventBindingSource;
        private _17033_DBDataSet1TableAdapters.EventTableAdapter eventTableAdapter;
        private System.Windows.Forms.ComboBox podsjetnikEventa;
        private System.Windows.Forms.Button povratakNaGlavnuFormu;
        private _17033_DBDataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView eventDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DateTimePicker vrijemePocetkaEventa;
        private System.Windows.Forms.DateTimePicker vrijemeZavrsetkaEventa;
    }
}