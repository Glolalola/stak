﻿using System;
using System.Collections.Generic;
using System.Data.Common;

namespace STAK
{
    public class Dogadaj
    {

        private DateTime  datumDogadaja;
        private string  nazivDogadaja;
        private DateTime vrijemePocetkaDogadaja;
        private DateTime vrijemeZavrsetkaDogadaja;
        private string opisDogadaja;
        private string lokacijaDogadaja;
        private DateTime podsjetnikDogadaja;
        

        Dogadaj()
        {

        }
        public Dogadaj(DbDataReader dr)
        {
            if (dr != null)
            {
                
                naziv = dr["name"].ToString();
                datum = DateTime.Parse(dr["eventDate"].ToString());
                vrijemePocetkaDogadaja = DateTime.Parse(dr["startTime"].ToString());
                vrijemeZavrsetkaDogadaja = DateTime.Parse(dr["endTime"].ToString());
                opisDogadaja = dr["description"].ToString();
                lokacijaDogadaja = dr["location"].ToString();
                podsjetnikDogadaja = DateTime.Parse(dr["reminder"].ToString());

            }
        }
        public Dogadaj(DateTime datum, string naziv, DateTime pocetak, DateTime zavrsetak, string opis, string lokacija, DateTime podsjetnik)
        {
            datumDogadaja = datum;
            nazivDogadaja = naziv;
            vrijemePocetkaDogadaja = pocetak;
            vrijemeZavrsetkaDogadaja = zavrsetak;
            opisDogadaja = opis;
            lokacijaDogadaja = lokacija;
            podsjetnikDogadaja = podsjetnik;
        }
        public DateTime datum
        {
            get
            {
                return datumDogadaja;
            }
            private set
            {
                if (datumDogadaja != value)
                {
                    datumDogadaja = value;
                }
            }
        }
        public string naziv
        {
            get
            {
                return nazivDogadaja;
            }
            private set
            {
                if (nazivDogadaja != value)
                {
                    nazivDogadaja = value;
                }
            }
        }
        public DateTime vrijemePocetka
        {
            get
            {
                return vrijemePocetkaDogadaja;
            }
            private set
            {
                if (vrijemePocetkaDogadaja != value)
                {
                    vrijemePocetkaDogadaja = value;
                }
            }
        }

        public DateTime vrijemeZavrsetka
        {
            get
            {
                return vrijemeZavrsetkaDogadaja;
            }
            private set
            {
                if (vrijemeZavrsetkaDogadaja != value)
                {
                    vrijemeZavrsetkaDogadaja = value;
                }
            }
        }

        public string opis
        {
            get
            {
                return opisDogadaja;
            }
            private set
            {
                if (opisDogadaja != value)
                {
                    opisDogadaja = value;
                }
            }
        }

        public string lokacija
        {
            get
            {
                return lokacijaDogadaja;
            }
            private set
            {
                if (lokacijaDogadaja != value)
                {
                    lokacijaDogadaja = value;
                }
            }
        }


        public DateTime podsjetnik
        {
            get
            {
                return podsjetnikDogadaja;
            }
            private set
            {
                if (podsjetnikDogadaja != value)
                {
                    podsjetnikDogadaja = value;
                }
            }
        }

        public bool Spremi()
        {
            
                string sqlUpit = "";

                sqlUpit = " INSERT INTO Event  (name , eventDate , startTime , endTime , description, location, reminder ) VALUES( '" + nazivDogadaja + "', '" + datumDogadaja.ToString("yyyy-MM-dd") + "', '" + vrijemePocetkaDogadaja.ToString("HH:mm:ss.fff") + "' , '" + vrijemeZavrsetkaDogadaja.ToString("HH:mm:ss.fff") + "'  , '" + opisDogadaja + "'  , '" + lokacijaDogadaja + "'  ,'" + podsjetnikDogadaja.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' ) ";
                DB.Instance.IzvrsiUpit(sqlUpit);

                return true;

        }
        public static List<Dogadaj> DohvatiSveDogadaje()
        {
            List<Dogadaj> lista = new List<Dogadaj>();
            string sqlUpit = "SELECT * FROM Event";
            DbDataReader dr = DB.Instance.DohvatiDataReader(sqlUpit);
            while (dr.Read())
            {
                Dogadaj dogadaj = new Dogadaj(dr);
                lista.Add(dogadaj);
            }
            dr.Close();
            return lista;
        }




    }
}
