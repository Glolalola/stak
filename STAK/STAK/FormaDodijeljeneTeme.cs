﻿using System;
using System.Windows.Forms;

namespace STAK
{
    public partial class FormaDodijeljeneTeme : Form
    {
        public FormaDodijeljeneTeme()
        {
            InitializeComponent();
        }
        private void PovratakNaGlavniIzbornik_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlavnaForma GlavnaForma = new GlavnaForma();
            GlavnaForma.Show();
        }
        private void FormaDodijeljeneTeme_Load(object sender, EventArgs e)
        {
            this._17033_DBDataSet2.EnforceConstraints = false;
            this.pregledTemaTableAdapter.Fill(this._17033_DBDataSet2.PregledTema);
        }
        private void FormaDodijeljeneTeme_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"https://github.com/foivz/r17033/wiki/Korisni%C4%8Dka-dokumentacija", "user-content-pregled-tema");
        }
    }
}
