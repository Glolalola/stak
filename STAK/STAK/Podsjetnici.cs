﻿
using System.Collections.Generic;


namespace STAK
{
    public  class Podsjetnici
    {

        public string imePodsjetnika;
        public int vrijednostPodsjetnika;

     

        public int vrijednost
        {
            get
            {
                return vrijednostPodsjetnika;
            }
            private set
            {
                if (vrijednostPodsjetnika != value)
                {
                    vrijednostPodsjetnika = value;
                }
            }
        }


        public string ime
        {
            get
            {
                return imePodsjetnika;
            }
            private set
            {
                if (imePodsjetnika != value)
                {
                    imePodsjetnika = value;
                }
            }
        }

        Podsjetnici(string ime, int vrijednost)
        {
            imePodsjetnika = ime;
            vrijednostPodsjetnika = vrijednost;
            
        }

        public static List<Podsjetnici> listaPodsjetnika = new List<Podsjetnici>()
        {
            new Podsjetnici("Dan prije", -24*60),
            new Podsjetnici("Sat prije", -1*60),
            new Podsjetnici("Dva sata prije", -2*60),
            new Podsjetnici("Minutu prije", -1)

        };

        


    }
}
