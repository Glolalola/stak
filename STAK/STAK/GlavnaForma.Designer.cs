﻿namespace STAK
{
    partial class GlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GlavnaForma));
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.idiNaKalendar = new System.Windows.Forms.Panel();
            this.idiNaDodijeljeneTeme = new System.Windows.Forms.Panel();
            this.idiNaForum = new System.Windows.Forms.Panel();
            this.idiNaUrednickuAdministraciju = new System.Windows.Forms.Panel();
            this.idiNaSlanjeObavijesti = new System.Windows.Forms.Panel();
            this.odjaviSe = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // idiNaKalendar
            // 
            this.idiNaKalendar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.idiNaKalendar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("idiNaKalendar.BackgroundImage")));
            this.idiNaKalendar.Location = new System.Drawing.Point(391, 177);
            this.idiNaKalendar.Name = "idiNaKalendar";
            this.idiNaKalendar.Size = new System.Drawing.Size(100, 100);
            this.idiNaKalendar.TabIndex = 8;
            this.idiNaKalendar.Click += new System.EventHandler(this.idiNaKalendar_Click);
            // 
            // idiNaDodijeljeneTeme
            // 
            this.idiNaDodijeljeneTeme.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.idiNaDodijeljeneTeme.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("idiNaDodijeljeneTeme.BackgroundImage")));
            this.idiNaDodijeljeneTeme.Location = new System.Drawing.Point(289, 283);
            this.idiNaDodijeljeneTeme.Name = "idiNaDodijeljeneTeme";
            this.idiNaDodijeljeneTeme.Size = new System.Drawing.Size(100, 100);
            this.idiNaDodijeljeneTeme.TabIndex = 9;
            this.idiNaDodijeljeneTeme.Click += new System.EventHandler(this.idiNaDodijeljeneTeme_Click);
            // 
            // idiNaForum
            // 
            this.idiNaForum.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.idiNaForum.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("idiNaForum.BackgroundImage")));
            this.idiNaForum.Location = new System.Drawing.Point(493, 283);
            this.idiNaForum.Name = "idiNaForum";
            this.idiNaForum.Size = new System.Drawing.Size(100, 100);
            this.idiNaForum.TabIndex = 10;
            this.idiNaForum.Click += new System.EventHandler(this.idiNaForum_Click);
            // 
            // idiNaUrednickuAdministraciju
            // 
            this.idiNaUrednickuAdministraciju.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.idiNaUrednickuAdministraciju.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("idiNaUrednickuAdministraciju.BackgroundImage")));
            this.idiNaUrednickuAdministraciju.Location = new System.Drawing.Point(697, 283);
            this.idiNaUrednickuAdministraciju.Name = "idiNaUrednickuAdministraciju";
            this.idiNaUrednickuAdministraciju.Size = new System.Drawing.Size(100, 100);
            this.idiNaUrednickuAdministraciju.TabIndex = 11;
            this.idiNaUrednickuAdministraciju.Click += new System.EventHandler(this.idiNaUrednickuAdministraciju_Click);
            // 
            // idiNaSlanjeObavijesti
            // 
            this.idiNaSlanjeObavijesti.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.idiNaSlanjeObavijesti.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("idiNaSlanjeObavijesti.BackgroundImage")));
            this.idiNaSlanjeObavijesti.Location = new System.Drawing.Point(187, 177);
            this.idiNaSlanjeObavijesti.Name = "idiNaSlanjeObavijesti";
            this.idiNaSlanjeObavijesti.Size = new System.Drawing.Size(100, 100);
            this.idiNaSlanjeObavijesti.TabIndex = 13;
            this.idiNaSlanjeObavijesti.Click += new System.EventHandler(this.idiNaSlanjeObavijesti_Click);
            // 
            // odjaviSe
            // 
            this.odjaviSe.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.odjaviSe.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("odjaviSe.BackgroundImage")));
            this.odjaviSe.Location = new System.Drawing.Point(595, 177);
            this.odjaviSe.Name = "odjaviSe";
            this.odjaviSe.Size = new System.Drawing.Size(100, 100);
            this.odjaviSe.TabIndex = 14;
            this.odjaviSe.Click += new System.EventHandler(this.odjaviSe_Click);
            // 
            // GlavnaForma
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.odjaviSe);
            this.Controls.Add(this.idiNaSlanjeObavijesti);
            this.Controls.Add(this.idiNaUrednickuAdministraciju);
            this.Controls.Add(this.idiNaForum);
            this.Controls.Add(this.idiNaDodijeljeneTeme);
            this.Controls.Add(this.idiNaKalendar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GlavnaForma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "STAK";
            this.Load += new System.EventHandler(this.GlavnaForma_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.GlavnaForma_HelpRequested);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.Panel idiNaKalendar;
        private System.Windows.Forms.Panel idiNaDodijeljeneTeme;
        private System.Windows.Forms.Panel idiNaForum;
        private System.Windows.Forms.Panel idiNaUrednickuAdministraciju;
        private System.Windows.Forms.Panel idiNaSlanjeObavijesti;
        private System.Windows.Forms.Panel odjaviSe;
    }
}