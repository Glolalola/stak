﻿namespace STAK
{
    partial class FormaNoviKomentar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaNoviKomentar));
            this.objaviPost = new System.Windows.Forms.Button();
            this.sadrzajNoviKomentar = new System.Windows.Forms.Label();
            this.tekstPosta = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // objaviPost
            // 
            this.objaviPost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.objaviPost.BackColor = System.Drawing.SystemColors.Control;
            this.objaviPost.Location = new System.Drawing.Point(630, 414);
            this.objaviPost.Name = "objaviPost";
            this.objaviPost.Size = new System.Drawing.Size(105, 23);
            this.objaviPost.TabIndex = 8;
            this.objaviPost.Text = "Objavi";
            this.objaviPost.UseVisualStyleBackColor = false;
            // 
            // sadrzajNoviKomentar
            // 
            this.sadrzajNoviKomentar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sadrzajNoviKomentar.AutoSize = true;
            this.sadrzajNoviKomentar.BackColor = System.Drawing.Color.Transparent;
            this.sadrzajNoviKomentar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sadrzajNoviKomentar.Location = new System.Drawing.Point(249, 123);
            this.sadrzajNoviKomentar.Name = "sadrzajNoviKomentar";
            this.sadrzajNoviKomentar.Size = new System.Drawing.Size(95, 13);
            this.sadrzajNoviKomentar.TabIndex = 7;
            this.sadrzajNoviKomentar.Text = "Sadržaj komentara";
            // 
            // tekstPosta
            // 
            this.tekstPosta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tekstPosta.Location = new System.Drawing.Point(249, 155);
            this.tekstPosta.Name = "tekstPosta";
            this.tekstPosta.Size = new System.Drawing.Size(475, 232);
            this.tekstPosta.TabIndex = 6;
            this.tekstPosta.Text = "";
            // 
            // FormaNoviKomentar
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::STAK.Properties.Resources.pozadina_789_;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.objaviPost);
            this.Controls.Add(this.sadrzajNoviKomentar);
            this.Controls.Add(this.tekstPosta);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormaNoviKomentar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormaNoviKomentar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button objaviPost;
        private System.Windows.Forms.Label sadrzajNoviKomentar;
        private System.Windows.Forms.RichTextBox tekstPosta;
    }
}